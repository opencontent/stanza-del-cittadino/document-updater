# Document Updater

Questo componente è parte del sistema di generazione dei documenti a partire dalle relative pratiche per permettere la protocollazione in maniera indipendente.  

### Cosa fa
il document updater funge da anti-corruption layer tra il dominio dei documenti e il dominio delle application.
Esso ha il compito di leggere dal topic documents, e per ogni documento protocollato, deve aggiornare di conseguenza la relativa pratica sulla Stanza del Cittadino attraverso le sue API

## Struttura del repository
Il repository ha come unico riferimento il branch main. \
La struttura di base riprende i principi del hexagonal architecture in cui:\
**main.go** - entrypoint del servizio. Qui vengono assemblate e avviate le strutture seguenti.\
**intenrnal/core/domain**: - contiene le entità utilizzate dalla business logic"\
**internal/core/services**: - contiente la business logic\
**internal/core/ports**: - contiente le intefacce che descrivono come i driver e i driven interagiscono la busines logic\
**internal/adapters/drivers**: - contiene i driver, ovvero le tecnologie esposte dal servizio per poter essere richiamati dall'esterno \
**internal/adapters/repositories**: - contiene i driven, ovvero le tecnologie utilizzate dal servizio per poter accedere al mondo esterno \
**config/** - contiene la struttura atta alla configurazione del servizio: variabili d'ambiente, configurazione hardcoded\
**metrics/** - contiene la descrizione delle metriche Prometheus esposte dal servizio\
**sentryUtils/** - contiene la configurazione di Sentry per il monitoring degli errori\
**logger/** - contiene la configurazione del sistema di logs in formato in formato Apache\
**utils/** - contiene le utils del codice(funzioni accessorie..) \
Altri File: \
file di configurazione per la continuous integration:\ 
dockerFile, docker-compose.yml, docker-compose.ovverride.yml, gitlab-ci.yml, dockerignore, gitignore\

## Prerequisiti e dipendenze
tutte le librerie esterne utilizzate sono elencate nel file go.mod. La sola presenza di tale file e del suo attuale contenuto permette una gestione automatica delle dipendenze. \
È necessaria l'installazione di Docker e Docker Compose per il deploy automatico del servizio e di tutti gli applicativi accessori(kafka, kafka-ui, ksqldb-server, ksqldb-cli, zookeeper..) 

## Monitoring
il sistema usufruisce di Sentry per il monitoring degli errori, inoltre sono esposti i seguenti endpoint per il monitoring : \
- /status : endpoint per l'healtcheck. Resituisce 200
- /metrics : espone le metriche in formato Prometheus

### Configurazione variabili d'ambiente
| Nome                             | Default | Descrizione                                                             |
|----------------------------------|---------|-------------------------------------------------------------------------|
| APP_VERSION                      | test    | versione del servizio                                                   |
| ENVIRONMENT                      | test    | indica l'ambiente di sviluppo(locale, dev,prod...utilizzato da sentry)  | 
| APPLICATION_EVENT_VERSION        | 0       | versione dell'evento di tipo application                                |
| KAFKA_SERVER                     | test    | indirizzo del broker kafka per connetersi al cluster                    |
| KAFKA_CONSUMER_GROUP             | test    | consumer group                                                          |
| KAFKA_CONSUMER_TOPIC             | test    | identifica il topic da cui consumare gli eventi                         |
| SERVER_ADDRESS_PORT              | test    | indica l'indirizzo e la porta utilizzati per l'healthcheck              |
| HEALTHCHECK_ENDPOINT             | test    | indica l'endpoint utilizzato per l'heathcheck                           |
| SENTRY_DSN                       | test    | endpoint per il monitoring di sentry                                    |
| ENDPOINT_METRICS                 | test    | endpoint per accedere alle metriche di Prometheus                       |
| SDC_BASENAME                     | test    | endpoint stanza del cittadino                                           |
| SDC_AUTH_TOKEN_USER              | test    | username per recuperare il token per l'utilizzo delle API               |
| SDC_AUTH_TOKEN_PASSWORD          | test    | password per recuperare il token per l'utilizzo delle API               |


## Come si usa
Aggiungere il file docker-compose.override.yml avente seguente struttura : 
```yaml
version: '<la tua versione>'

networks:
  core_default:
    external: true

services:
  kafka:
    networks:
      - core_default
      - default

  documentupdater:
    build:
      context: '<il tuo contesto>'
    environment:
      APP_VERSION: '<la tua versione dell'app>'
      ENVIRONMENT: '<il tuo ambiente>'

      KAFKA_SERVER: 'fake1,kafka:9092,fake2'
      KAFKA_CONSUMER_GROUP: '<il tuo gruppo di consumatori Kafka>'
      KAFKA_CONSUMER_TOPIC: '<il tuo topic di consumatori Kafka>'
      KAFKA_PRODUCER_TOPIC: 'retryQueue'

      SERVER_ADDRESS_PORT: '0.0.0.0:9090'
      ENDPOINT_HEALTHCHECK: '/status'
      ENDPOINT_METRICS: '/metrics'
      SENTRY_DSN: '<il tuo SENTRY_DSN>'

      SDC_BASENAME: 'endpoint Stanza Del Cittadino'
      SDC_AUTH_TOKEN_USER: 'user '
      SDC_AUTH_TOKEN_PASSWORD: 'password'

    ports:
      - "<la tua porta esterna>:9090"
    volumes:
      - '<il tuo percorso al file JSON dei tenant>:/mocks/tenantsList.json'
    networks:
      - core_default
      - default
```

Da terminale: 
1. `git clone  git@gitlab.com:opencity-labs/area-personale/document-updater.git`
2. `cd document-updater`
3. `docker-compose build`
4. `docker compose up -d`

## Testing
sono presenti unit test. per lanciarli, utilizzare il commando `go test .`

## Stadio di sviluppo

il software si trova in fase `Feature-complete`


## Autori e Riconoscimenti

* Opencity Labs

## License

AGPL 3.0 only

## Contatti

support@opencitylabs.it


