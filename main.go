package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/internal/adapters/drivers"
	"opencitylabs.it/documentupdater/internal/adapters/repositories"
	"opencitylabs.it/documentupdater/internal/core/services"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/sentryutils"
)

func main() {
	config := config.GetConfig()
	logger := logger.GetLogger()
	ctx, exitFn := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	err := sentryutils.SentryInit(config.Sentry.Dsn, config.App.Version, config.App.Environment)
	if err != nil {
		logger.Error("sentry.Init: ", zap.Error(err))
		sentry.CaptureException(err)
	}

	//composition root: all dependencies are tieg together here
	retryHandler := repositories.NewRetryHandler(ctx)
	sdcClient := repositories.NewHttpClient()
	service := services.NewService(retryHandler, sdcClient)
	kafkaConsumerDriver := drivers.NewKafkaConsumer(service)
	defer kafkaConsumerDriver.CloseKafkaReader()

	//metrics and healthcheck
	server := drivers.NewHttpServer()

	wg.Add(2)
	go func() {
		defer wg.Done()
		logger.Debug("server listening on " + config.Server.AddressPort)
		server.Run()
	}()

	//core logic entry point
	go func() {
		defer wg.Done()
		kafkaConsumerDriver.ProcessMessage(ctx)
	}()

	// gracefully shutdown
	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	wg.Wait()
	exitFn()
	sentry.Flush(2 * time.Second)
	_ = logger.Sync()
	// a timeout of 15 seconds to shutdown the server
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	kafkaConsumerDriver.CloseKafkaReader()

	logger.Debug("all processes done, shutting down")

}
