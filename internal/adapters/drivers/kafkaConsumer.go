package drivers

import (
	"context"
	"errors"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/internal/core/ports"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/metrics"
	"opencitylabs.it/documentupdater/sentryutils"
	"opencitylabs.it/documentupdater/utils"
)

type KafkaConsumer struct {
	KafkaReader *kafka.Reader
	log         *zap.Logger
	config      *config.Config
	service     ports.DocumentUpdaterServiceService
	sentryHub   *sentry.Hub
}

func NewKafkaConsumer(service ports.DocumentUpdaterServiceService) *KafkaConsumer {
	r := &KafkaConsumer{
		log:       logger.GetLogger(),
		config:    config.GetConfig(),
		service:   service,
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.KafkaReader = r.getKafkaReader(r.config.Kafka.KafkaServer, r.config.Kafka.ConsumerTopic, r.config.Kafka.ConsumerGroup)
	return r
}

func (r *KafkaConsumer) getKafkaReader(kafkaServer []string, kafkaTopic, consumerGroup string) *kafka.Reader {
	r.log.Sugar().Debug("Connecting to topic: ", kafkaTopic, " from kafka server ", kafkaServer)
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: kafkaServer,
		Topic:   kafkaTopic,
		GroupID: consumerGroup,
		MaxWait: 3 * time.Second,
	})
	return reader
}

func (r *KafkaConsumer) CloseKafkaReader() {
	err := r.KafkaReader.Close()
	if err != nil {
		r.log.Error("Error closing consumer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	r.log.Debug("Consumer closed")
}

func (r *KafkaConsumer) ProcessMessage(ctx context.Context) {
	for {
		m, err := r.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			r.log.Error("error reading message from kafka",
				zap.Error(err),
				zap.String("environment", r.config.App.Environment),
				zap.String("key", string(m.Key)),
			)
			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("environment", r.config.App.Environment)
				scope.SetTag("key", string(m.Key))
				sentry.CaptureException(errors.New("error reading message from kafka"))
			})
			continue
		}

		r.log.Debug("message key",
			zap.String("key", string(m.Key)),
		)

		document, err := utils.GetDocumentStruct(m.Value)
		if err != nil {
			event, _ := utils.GetEventIDs(m.Value)
			r.log.Info("Error building document struct",
				zap.String("event_id", event.EventID),
				zap.String("id", event.ID),
				zap.Error(err),
			)

			r.log.Error("error building document struct",
				zap.Error(err),
				zap.String("environment", r.config.App.Environment),
				zap.String("document_id", event.ID),
				zap.String("event_id", event.EventID),
			)
			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("environment", r.config.App.Environment)
				scope.SetTag("document_id", event.ID)
				scope.SetTag("event_id", event.EventID)
				sentry.CaptureException(errors.New("error building document struct"))
			})
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()
			r.log.Info("error building document struct",
				zap.String("environment", r.config.App.Environment),
				zap.String("document_id", event.ID),
				zap.String("event_id", event.EventID),
			)

			continue
		}

		err = r.service.ProcessMessage(&document)
		if err != nil {
			r.log.Error("error processing document",
				zap.Error(err),
				zap.String("environment", r.config.App.Environment),
				zap.String("document_id", document.ID),
				zap.String("tenant_id", document.TenantID),
			)
			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("environment", r.config.App.Environment)
				scope.SetTag("document_id", document.ID)
				scope.SetTag("tenant_id", document.TenantID)
				sentry.CaptureException(errors.New("error processing document"))
			})

			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()

			//add event to retryQueue
			err = r.service.AddToRetryMechanism(m.Value)
			if err != nil {
				r.log.Error("failed adding document on retry mechanism",
					zap.Error(err),
					zap.String("environment", r.config.App.Environment),
					zap.String("document_id", document.ID),
					zap.String("tenant_id", document.TenantID),
				)
				sentry.WithScope(func(scope *sentry.Scope) {
					scope.SetTag("environment", r.config.App.Environment)
					scope.SetTag("document_id", document.ID)
					scope.SetTag("tenant_id", document.TenantID)
					sentry.CaptureException(errors.New("failed adding document on retry mechanism"))
				})

				r.log.Info("failed adding document on retry mechanism",
					zap.String("environment", r.config.App.Environment),
					zap.String("document_id", document.ID),
					zap.String("tenant_id", document.TenantID),
				)
				continue
			}

			r.log.Info("added to retry queue",
				zap.String("environment", r.config.App.Environment),
				zap.String("document_id", document.ID),
				zap.String("tenant_id", document.TenantID),
			)
			continue
		}

		err = r.KafkaReader.CommitMessages(ctx, m)
		if err != nil {
			r.log.Error("failed to commit event",
				zap.Error(err),
				zap.String("environment", r.config.App.Environment),
				zap.String("document_id", document.ID),
				zap.String("tenant_id", document.TenantID),
			)
			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("environment", r.config.App.Environment)
				scope.SetTag("document_id", document.ID)
				scope.SetTag("tenant_id", document.TenantID)
				sentry.CaptureException(errors.New("failed to commit event"))
			})
			r.log.Info("added to retry queue",
				zap.String("environment", r.config.App.Environment),
				zap.String("document_id", document.ID),
				zap.String("tenant_id", document.TenantID),
			)
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()
			continue
		}
		r.log.Debug("message committed",
			zap.String("document_id", document.ID),
		)

		r.log.Info("processed correctly",
			zap.String("environment", r.config.App.Environment),
			zap.String("document_id", document.ID),
			zap.String("tenant_id", document.TenantID),
		)
		metrics.OpsSuccessProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()

	}
}
