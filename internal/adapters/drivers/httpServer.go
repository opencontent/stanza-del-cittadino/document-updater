package drivers

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/logger"
)

type registryProxyServer struct {
	log  *zap.Logger
	conf *config.Config
	srv  *gin.Engine
}

func NewHttpServer() *registryProxyServer {
	config := config.GetConfig()
	s := &registryProxyServer{
		log:  logger.GetLogger(),
		conf: config,
	}
	gin.SetMode(gin.ReleaseMode)

	if !config.Server.Debug {
		gin.DefaultWriter = io.Discard
	}
	s.srv = gin.Default()
	s.srv.ForwardedByClientIP = true

	s.srv.SetTrustedProxies([]string{s.conf.Server.AddressPort})
	s.initRoutes()

	return s
}

func (r *registryProxyServer) initRoutes() {
	r.srv.GET("/status", r.healthCheckHandler)
	r.srv.GET("/metrics", gin.WrapH(promhttp.Handler()))
}

func (r *registryProxyServer) Run() {
	r.srv.Run(r.conf.Server.AddressPort)
}

func (r *registryProxyServer) healthCheckHandler(c *gin.Context) {
	c.String(http.StatusOK, "Healthy")
}
