package drivers

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/logger"
)

func SetUpRouter() *gin.Engine {
	router := gin.Default()
	return router
}

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestHeathCheckHandler(t *testing.T) {

	logger := logger.GetLogger()
	defer logger.Sync()
	registryProxyServer := registryProxyServer{
		log:  logger,
		conf: config.GetConfig(),
	}

	expectedResult := `Healthy`
	r := SetUpRouter()
	r.GET("/status", registryProxyServer.healthCheckHandler)
	responseResult := performRequest(r, "GET", "/status")
	responseDataResult, _ := io.ReadAll(responseResult.Body)

	assert.Equal(t, http.StatusOK, responseResult.Code)
	assert.Equal(t, expectedResult, string(responseDataResult))
	assert.Equal(t, http.StatusOK, responseResult.Code)
}
