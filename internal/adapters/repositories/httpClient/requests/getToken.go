package requests

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/metrics"
)

type GetAuthToken struct {
	client *http.Client
	log    *zap.Logger
	conf   *config.Config
}

func NewGetAuthToken() GetAuthToken {
	return GetAuthToken{
		log:    logger.GetLogger(),
		conf:   config.GetConfig(),
		client: &http.Client{},
	}
}

func (r *GetAuthToken) GetAuthToken(basePath string) (string, error) {
	token, err := r.doGetAuthTokenRequest(basePath)
	if err != nil {
		return "", err
	}
	return token, nil
}

func (r *GetAuthToken) doGetAuthTokenRequest(basePath string) (string, error) {

	req, err := r.buildRequest(basePath)
	if err != nil {
		return "", err
	}
	timer := prometheus.NewTimer(metrics.MetricsSdcLatency.WithLabelValues(r.conf.App.Environment, r.conf.App.AppName, req.URL.RequestURI()))

	resp, err := r.client.Do(req)
	if err != nil {
		r.log.Sugar().Error("error performing sdcGetAuthToken request: ", zap.Error(err))
		return "", err
	}
	timer.ObserveDuration()
	defer resp.Body.Close()
	token, err := r.handleAuthTokenResponse(resp)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (r *GetAuthToken) buildRequest(basePath string) (*http.Request, error) {
	url := basePath + "auth"
	data := map[string]string{
		"username": r.conf.Client.SdcAuthTokenUser,
		"password": r.conf.Client.SdcAuthTokenPsw,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		r.log.Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.log.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

func (r *GetAuthToken) handleAuthTokenResponse(resp *http.Response) (string, error) {

	if resp.StatusCode != http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			r.log.Error("error decoding body AuthToken response: ", zap.Error(err))
		}

		r.log.Sugar().Debug("bad AuthToken response: ", string(bodyBytes))
		return "", errors.New("auth token bad error response")
	}
	token := token{}
	err := json.NewDecoder(resp.Body).Decode(&token)
	if err != nil {
		r.log.Sugar().Error("error decoding body auth response: ", zap.Error(err))
		return "", err
	}
	return token.Value, nil
}

type token struct {
	Value string `json:"token"`
}
