package requests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/internal/core/domain"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/metrics"
)

type IRegisterIntegrationRequest interface {
	RegisterIntegrationRequest(token string, document *domain.Document) error
}

type registerIntegrationRequest struct {
	client   *http.Client
	log      *zap.Logger
	conf     *config.Config
	basepath string
	url      string
	payload  string
}

func NewRegisterIntegrationRequest(basepath string) IRegisterIntegrationRequest {
	return &registerIntegrationRequest{
		log:      logger.GetLogger(),
		conf:     config.GetConfig(),
		client:   &http.Client{},
		basepath: basepath,
	}
}

func (r *registerIntegrationRequest) RegisterIntegrationRequest(token string, document *domain.Document) error {
	err := r.doRequest(token, document)
	if err != nil {
		return err
	}

	return nil
}

func (r *registerIntegrationRequest) doRequest(token string, document *domain.Document) error {
	req, err := r.buildRequest(token, document)
	if err != nil {
		return err
	}

	timer := prometheus.NewTimer(metrics.MetricsSdcLatency.WithLabelValues(r.conf.App.Environment, r.conf.App.AppName, req.URL.RequestURI()))
	resp, err := r.client.Do(req)
	if err != nil {
		r.log.Error("error performing RegisterIntegrationRequest request",
			zap.Error(err),
			zap.String("environment", r.conf.App.Environment),
			zap.String("app_name", r.conf.App.AppName),
			zap.String("url", req.URL.RequestURI()),
		)
		return err
	}
	timer.ObserveDuration()

	defer resp.Body.Close()

	err = r.handleResponse(resp)
	if err != nil {
		return err
	}

	return nil

}

func (r *registerIntegrationRequest) buildRequest(token string, document *domain.Document) (*http.Request, error) {
	r.url = r.basepath + "applications/" + *document.RemoteID + "/transition/register-integration-request"

	payloadValues := r.getPayloadValues(document)
	payload, err := json.Marshal(payloadValues)
	if err != nil {
		r.log.Error("Error Marshalling data",
			zap.Error(err),
			zap.String("environment", r.conf.App.Environment),
			zap.String("app_name", r.conf.App.AppName),
			zap.String("document_id", *document.RemoteID),
		)
		return nil, err
	}
	r.payload = string(payload)
	req, err := http.NewRequest(http.MethodPost, r.url, bytes.NewBuffer(payload))
	if err != nil {
		r.log.Error("Error making new request",
			zap.Error(err),
			zap.String("environment", r.conf.App.Environment),
			zap.String("app_name", r.conf.App.AppName),
			zap.String("url", r.url),
			zap.String("payload", r.payload),
		)
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}

func (r *registerIntegrationRequest) handleResponse(resp *http.Response) error {
	if resp.StatusCode == http.StatusUnprocessableEntity {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			r.log.Error("error decoding body response",
				zap.Error(err),
				zap.String("environment", r.conf.App.Environment),
				zap.String("app_name", r.conf.App.AppName),
				zap.String("url", r.url),
				zap.String("payload", r.payload),
			)
			return err
		}
		r.log.Debug("handle unprocessable entity response",
			zap.String("url", r.url),
			zap.String("payload", r.payload),
			zap.Int("status_code", resp.StatusCode),
			zap.String("response_body", string(bodyBytes)),
			zap.String("environment", r.conf.App.Environment),
			zap.String("app_name", r.conf.App.AppName),
		)
		return nil
	}

	if resp.StatusCode != http.StatusNoContent {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			r.log.Error("error decoding body RegisterIntegrationRequest response",
				zap.Error(err),
				zap.String("environment", r.conf.App.Environment),
				zap.String("app_name", r.conf.App.AppName),
				zap.String("url", r.url),
				zap.String("payload", r.payload),
				zap.Int("status_code", resp.StatusCode),
			)
			return err
		}
		r.log.Debug("RegisterIntegrationRequest bad error response",
			zap.String("url", r.url),
			zap.String("payload", r.payload),
			zap.Int("status_code", resp.StatusCode),
			zap.String("response_body", string(bodyBytes)),
			zap.String("environment", r.conf.App.Environment),
			zap.String("app_name", r.conf.App.AppName),
		)
		return errors.New("RegisterIntegrationRequest bad error response. Status code: " + fmt.Sprint(resp.StatusCode))
	}
	r.log.Debug("RegisterIntegrationRequest successful response",
		zap.String("url", r.url),
		zap.String("payload", r.payload),
		zap.Int("status_code", resp.StatusCode),
		zap.String("environment", r.conf.App.Environment),
		zap.String("app_name", r.conf.App.AppName),
	)
	return nil
}

func (r *registerIntegrationRequest) getPayloadValues(document *domain.Document) PayloadRegisterIntegrationRequest {
	var payload = PayloadRegisterIntegrationRequest{}

	payload.IntegrationOutBoundProtocolNumber = document.RegistrationData.DocumentNumber
	payload.IntegrationOutBoundProtocolledAt = document.RegistrationData.Date
	//payload.IntegrationOutBoundProtocolDocumentId = *document.RegistrationData.RegistryCode

	return payload
}

type PayloadRegisterIntegrationRequest struct {
	IntegrationOutBoundProtocolNumber string `json:"integration_outbound_protocol_number,omitempty"`
	//IntegrationOutBoundProtocolDocumentId string `json:"integration_outbound_protocol_document_id,omitempty"`
	IntegrationOutBoundProtocolledAt string `json:"integration_outbound_protocolled_at,omitempty"`
}
