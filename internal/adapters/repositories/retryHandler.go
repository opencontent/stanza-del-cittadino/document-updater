package repositories

import (
	"context"
	"encoding/json"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/internal/core/domain"
	"opencitylabs.it/documentupdater/internal/core/ports"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/sentryutils"
)

type retryHandler struct {
	log         *zap.Logger
	conf        *config.Config
	KafkaWriter *kafka.Writer
	ctx         context.Context
	sentryHub   *sentry.Hub
}

func NewRetryHandler(ctx context.Context) ports.RetryMechanismRepository {
	r := &retryHandler{
		log:       logger.GetLogger(),
		conf:      config.GetConfig(),
		ctx:       ctx,
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.KafkaWriter = r.getKafkaWriter(r.conf.Kafka.KafkaServer, r.conf.Kafka.ProducerTopic, r.conf.Kafka.ConsumerGroup)
	return r
}

func (r *retryHandler) AddToRetryMechanism(m []byte, document *domain.Document) error {
	originalTopic := r.conf.Kafka.ConsumerTopic
	// 1 parsare in una mappa
	var message map[string]interface{}
	retryMeta := make(map[string]interface{})

	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &message)
	if err != nil {
		r.log.Debug("error unmarshalling message into map")
		sentry.CaptureException(err)
		return err
	}
	if _, ok := message["retry_meta"]; !ok {
		retryMeta["original_topic"] = originalTopic
		message["retry_meta"] = retryMeta
	}

	messageReady := r.setEventFieldsBeforProduce(message)
	if err != nil {
		r.log.Error("", zap.Error(err))
		sentry.CaptureException(err)
		return err
	}

	documentByte, err := json.Marshal(messageReady)
	if err != nil {
		r.log.Debug("error unmarshalling message into []byte")
		sentry.CaptureException(err)
		return err
	}

	var eventKey string
	if document.RemoteCollection != nil {
		eventKey = document.RemoteCollection.ID
	} else {
		eventKey = ""
	}

	err = r.KafkaWriter.WriteMessages(r.ctx, kafka.Message{
		Key:   []byte(eventKey),
		Value: documentByte,
	})

	if err != nil {
		r.log.Error("could not write message: ", zap.Error(err))
		sentry.CaptureException(err)
		return err
	}
	r.log.Debug("document produced on topic",
		zap.String("document_id", document.ID),
		zap.String("producer_topic", r.conf.Kafka.ProducerTopic),
	)

	return nil
}
func (r *retryHandler) setEventFieldsBeforProduce(message map[string]interface{}) map[string]interface{} {

	message["app_id"] = "document-updater:" + config.VERSION

	currentTimeUTC := time.Now().UTC()
	romeLocation := time.FixedZone("Rome", 1*60*60)
	currentTimeRome := currentTimeUTC.In(romeLocation)
	iso8601Format := "2006-01-02T15:04:05+01:00"
	romeTime := currentTimeRome.Format(iso8601Format)
	message["event_created_at"] = romeTime

	message["updated_at"] = message["event_created_at"]
	newUuidEventId := uuid.NewString()
	message["event_id"] = newUuidEventId

	return message
}
func (r *retryHandler) getKafkaWriter(kafkaServer []string, kafkaTopic, producerGroup string) *kafka.Writer {
	r.log.Sugar().Debug("kafka producer: Connecting to topic: ", kafkaTopic, ":", " from kafka server ", kafkaServer)
	transport := &kafka.DefaultTransport
	kwriter := &kafka.Writer{
		Addr:      kafka.TCP(kafkaServer...),
		Topic:     kafkaTopic,
		Balancer:  &kafka.LeastBytes{},
		Transport: *transport,
	}
	return kwriter
}

func (r *retryHandler) CloseKafkaWriter() {
	err := r.KafkaWriter.Close()
	if err != nil {
		r.log.Error("Error closing producer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	r.log.Debug("producer closed")
}
