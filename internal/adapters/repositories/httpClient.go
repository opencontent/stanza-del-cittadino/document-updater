package repositories

import (
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/internal/adapters/repositories/httpClient/requests"
	"opencitylabs.it/documentupdater/internal/core/domain"
	"opencitylabs.it/documentupdater/internal/core/ports"
	"opencitylabs.it/documentupdater/logger"
)

type httpClient struct {
	log   *zap.Logger
	conf  *config.Config
	token requests.GetAuthToken
}

func NewHttpClient() ports.SdcClientRepository {
	return &httpClient{
		log:   logger.GetLogger(),
		conf:  config.GetConfig(),
		token: requests.NewGetAuthToken(),
	}
}

func (r *httpClient) RegisterApplication(basepath string, document domain.Document) error {
	token, err := r.token.GetAuthToken(basepath)
	if err != nil {
		return err
	}
	request := requests.NewRegisterApplication(basepath)
	err = request.RegisterApplication(token, &document)
	if err != nil {
		return err
	}
	return nil
}

func (r *httpClient) RegisterApplicationOutcome(basepath string, document domain.Document) error {
	token, err := r.token.GetAuthToken(basepath)
	if err != nil {
		return err
	}
	request := requests.NewRegisterApplicationOutcome(basepath)
	err = request.RegisterApplicationOutcome(token, &document)
	if err != nil {
		return err
	}
	return nil
}

func (r *httpClient) RegisterIntegrationOnApplicationRequest(basepath string, document domain.Document) error {
	token, err := r.token.GetAuthToken(basepath)
	if err != nil {
		return err
	}
	request := requests.NewRegisterIntegrationRequest(basepath)
	err = request.RegisterIntegrationRequest(token, &document)
	if err != nil {
		return err
	}
	return nil
}

func (r *httpClient) RegisterIntegrationOnApplicationAnswer(basepath string, document domain.Document) error {
	token, err := r.token.GetAuthToken(basepath)
	if err != nil {
		return err
	}
	request := requests.NewRegisterIntegrationAnswer(basepath)
	err = request.RegisterIntegrationAnswer(token, &document)
	if err != nil {
		return err
	}
	return nil
}

func (r *httpClient) RegisterApplicationWithdraw(basepath string, document domain.Document) error {
	token, err := r.token.GetAuthToken(basepath)
	if err != nil {
		return err
	}
	request := requests.NewRegisterApplicationWithdraw(basepath)
	err = request.RegisterApplicationWithdraw(token, &document)
	if err != nil {
		return err
	}
	return nil
}

func (r *httpClient) RegisterApplicationRevocation(basepath string, document domain.Document) error {
	token, err := r.token.GetAuthToken(basepath)
	if err != nil {
		return err
	}
	request := requests.NewRegisterApplicationRevocation(basepath)
	err = request.RegisterApplicationRevocation(token, &document)
	if err != nil {
		return err
	}
	return nil
}
