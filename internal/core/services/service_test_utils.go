package services

const DOCUMENT_FULL = `{
	"id": "dc06347f-93b0-488e-a359-17fe8c19fe16",
	"tenant_id": "cdb86081-c90c-4f30-8d92-78d66ad4770a",
	"remote_id": "418305e6-0e5d-4a14-8de1-7f423571fb4f",
	"type": {
		"name": "application",
		"child": {
			"name": "application-request"
		}
	},
	"collection": {
		"id": "f542da62-9796-419c-bcc6-d309c3fdeb4a",
		"type": "service"
	},
	"title": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024-Rosaria Crinò CRNRSR73C50F158S-418305e6-0e5d-4a14-8de1-7f423571fb4f",
	"user_id": "a717b215-4277-4c9f-a056-4d8948c2e33e",
	"author": {
		"type": "human",
		"tax_identification_number": "CRNRSR73C50F158S",
		"name": "Rosaria",
		"family_name": "Crinò",
		"street_name": null,
		"building_number": null,
		"postal_code": null,
		"town_name": null,
		"country_subdivision": null,
		"country": null,
		"email": null,
		"role": "sender"
	},
	"main_document": {
		"name": "modulo-richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a.s.20232024-202306300131.pdf",
		"filename": "649ebd00a8161.pdf",
		"description": "Modulo Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 2023-06-30 01:31",
		"download_urls": [
			{
				"type": "pdf",
				"url": "https://www2.stanzadelcittadino.it/comune-di-seriate/api/applications/518305e6-0e5d-4a14-8de1-7f423571fb4f/attachments/6dc1a86e-db17-45c9-8627-40d184920e17?version=1"
			}
		]
	},
	"attachments": [
		{
			"name": "verbale-accertamento-i.l.-6e1b377d-7a3d-4bb3-b921-8acd97e27794.pdf",
			"filename": "649ebc9b4828b732053343.pdf",
			"description": "Allegato",
			"download_urls": [
				{
					"type": "pdf",
					"url": "https://www2.stanzadelcittadino.it/comune-di-seriate/api/applications/518305e6-0e5d-4a14-8de1-7f423571fb4f/attachments/924cfa9c-0c81-4ada-a761-80a0308e56ee?version=1"
				}
			]
		},
		{
			"name": "diagnosi-funzionale-i.l._compressed-0fb9ea6f-cbd9-4147-af4a-eb8821365f66.pdf",
			"filename": "649ebca6c4fd5939170284.pdf",
			"description": "Allegato",
			"download_urls": [
				{
					"type": "pdf",
					"url": "https://www2.stanzadelcittadino.it/comune-di-seriate/api/applications/518305e6-0e5d-4a14-8de1-7f423571fb4f/attachments/23180c89-0161-48c8-a855-4cf75ee8fc41?version=1"
				}
			]
		}
	],
	"registration_data": {
		"transmission_type": null,
		"date": null,
		"document_number": "fake number",
		"registry_code": null
	},
	"folder": {
		"title": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024-Rosaria Crinò CRNRSR73C50F158S",
		"id": null
	},
	"created_at": "2023-06-30T13:31:26+02:00",
	"updated_at": null,
	"description": null,
	"short_description": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Rosaria Crinò CRNRSR73C50F158S",
	"document_url": null,
	"responsible_office": null,
	"available_formats": null,
	"distribution_license_id": null,
	"topics": null,
	"more_info": null,
	"normative_requirements": null,
	"related_documents": null,
	"related_services": null,
	"life_events": null,
	"business_events": null,
	"image": {},
	"image_gallery": null,
	"version": 1,
	"event_id": "e9bf89d5-fdfe-44b1-b89d-b0fcae29999a",
	"event_version": 2,
	"event_created_at": "2023-06-30T13:31:26+02:00",
	"app_id": "document-dispatcher-1.0.0",
	"visibility": "private"
}`

const DOCUMENT_MISSING_MANDATORY_FIELD = `{
	"id": "dc06347f-93b0-488e-a359-17fe8c19fe16",
	"tenant_id": "cdb86081-c90c-4f30-8d92-78d66ad4770a",
	"remote_id": "418305e6-0e5d-4a14-8de1-7f423571fb4f",
	"type": {
		"name": "application",
		"child": {
			"name": "application-request"
		}
	},
	"collection": {
		"id": "f542da62-9796-419c-bcc6-d309c3fdeb4a",
		"type": "service"
	},
	"title": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024-Rosaria Crinò CRNRSR73C50F158S-418305e6-0e5d-4a14-8de1-7f423571fb4f",
	"user_id": "a717b215-4277-4c9f-a056-4d8948c2e33e",
	"author": {
		"type": "human",
		"tax_identification_number": "CRNRSR73C50F158S",
		"name": "Rosaria",
		"family_name": "Crinò",
		"street_name": null,
		"building_number": null,
		"postal_code": null,
		"town_name": null,
		"country_subdivision": null,
		"country": null,
		"email": null,
		"role": "sender"
	},
	"main_document": {
		"name": "modulo-richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a.s.20232024-202306300131.pdf",
		"filename": "649ebd00a8161.pdf",
		"description": "Modulo Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 2023-06-30 01:31",
		"download_urls": [
			{
				"type": "pdf",
				"url": "https://www2.stanzadelcittadino.it/comune-di-seriate/api/applications/518305e6-0e5d-4a14-8de1-7f423571fb4f/attachments/6dc1a86e-db17-45c9-8627-40d184920e17?version=1"
			}
		]
	},
	"attachments": [
		{
			"name": "verbale-accertamento-i.l.-6e1b377d-7a3d-4bb3-b921-8acd97e27794.pdf",
			"filename": "649ebc9b4828b732053343.pdf",
			"description": "Allegato",
			"download_urls": [
				{
					"type": "pdf",
					"url": "https://www2.stanzadelcittadino.it/comune-di-seriate/api/applications/518305e6-0e5d-4a14-8de1-7f423571fb4f/attachments/924cfa9c-0c81-4ada-a761-80a0308e56ee?version=1"
				}
			]
		},
		{
			"name": "diagnosi-funzionale-i.l._compressed-0fb9ea6f-cbd9-4147-af4a-eb8821365f66.pdf",
			"filename": "649ebca6c4fd5939170284.pdf",
			"description": "Allegato",
			"download_urls": [
				{
					"type": "pdf",
					"url": "https://www2.stanzadelcittadino.it/comune-di-seriate/api/applications/518305e6-0e5d-4a14-8de1-7f423571fb4f/attachments/23180c89-0161-48c8-a855-4cf75ee8fc41?version=1"
				}
			]
		}
	],
	"registration_data": {
		"transmission_type": null,
		"date": null,
		"document_number": null,
		"registry_code": null
	},
	"folder": {
		"title": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024-Rosaria Crinò CRNRSR73C50F158S",
		"id": null
	},
	"created_at": "2023-06-30T13:31:26+02:00",
	"updated_at": null,
	"description": null,
	"document_url": null,
	"responsible_office": null,
	"available_formats": null,
	"distribution_license_id": null,
	"topics": null,
	"more_info": null,
	"normative_requirements": null,
	"related_documents": null,
	"related_services": null,
	"life_events": null,
	"business_events": null,
	"image": {},
	"image_gallery": null,
	"event_id": "e9bf89d5-fdfe-44b1-b89d-b0fcae29999a",
	"event_version": 2,
	"event_created_at": "2023-06-30T13:31:26+02:00",
	"app_id": "document-dispatcher-1.0.0",
	"visibility": "private"
}`
