package services

import (
	"errors"
	"fmt"
	"strings"

	"github.com/getsentry/sentry-go"
	"github.com/go-playground/validator"
	"go.uber.org/zap"
	"opencitylabs.it/documentupdater/config"
	"opencitylabs.it/documentupdater/internal/core/domain"
	"opencitylabs.it/documentupdater/internal/core/ports"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/metrics"
	"opencitylabs.it/documentupdater/sentryutils"
)

type service struct {
	log              *zap.Logger
	conf             *config.Config
	sentryHub        *sentry.Hub
	validator        *validator.Validate
	retryHandlerRepo ports.RetryMechanismRepository
	sdcClient        ports.SdcClientRepository
	document         *domain.Document
}

func NewService(producer ports.RetryMechanismRepository, client ports.SdcClientRepository) ports.DocumentUpdaterServiceService {
	return &service{
		log:              logger.GetLogger(),
		conf:             config.GetConfig(),
		sentryHub:        sentryutils.InitLocalSentryhub(),
		validator:        validator.New(),
		retryHandlerRepo: producer,
		sdcClient:        client,
	}
}

func (r *service) ProcessMessage(document *domain.Document) error {
	r.log.Debug("processing document",
		zap.String("environment", r.conf.App.Environment),
		zap.String("document_id", document.ID),
		zap.String("tenant_id", document.TenantID),
	)
	r.document = document
	err := r.validateDocumentMessage(document)
	if err != nil {
		return err
	}

	if document.Event_Version != r.conf.App.SupportedEventVersion {
		return nil
	}

	if !r.isDocumentProtocolled(document) {
		return nil
	}
	err = r.updateApplication(document)
	if err != nil {
		return err
	}

	return nil
}

func (r *service) validateDocumentMessage(document *domain.Document) error {
	err := r.validator.Struct(document)
	if err != nil {

		r.log.Error("error validating document: missing required fields",
			zap.Error(err),
			zap.String("environment", r.conf.App.Environment),
			zap.String("document_id", document.ID),
			zap.String("tenant_id", document.TenantID),
		)
		sentry.WithScope(func(scope *sentry.Scope) {
			scope.SetTag("environment", r.conf.App.Environment)
			scope.SetTag("document_id", document.ID)
			scope.SetTag("tenant_id", document.TenantID)
			sentry.CaptureException(errors.New("error validating document: missing required fields"))
		})

		metrics.OpsFailedProcessed.WithLabelValues(r.conf.App.Environment, document.TenantID).Inc()
		return err
	}
	return nil
}

func (r *service) isDocumentProtocolled(document *domain.Document) bool {
	if document.RegistrationData == nil {
		return false
	}
	return document.RegistrationData.DocumentNumber != ""
}

func (r *service) updateApplication(document *domain.Document) error {
	var err error

	baepath, err := r.getBasePath(document.MainDocument.URL)
	if err != nil {
		return err
	}
	switch document.Type {
	case "application-request":
		err = r.sdcClient.RegisterApplication(baepath, *document)
	case "application-outcome":
		err = r.sdcClient.RegisterApplicationOutcome(baepath, *document)
	case "integration-request":
		err = r.sdcClient.RegisterIntegrationOnApplicationRequest(baepath, *document)
	case "integration-response":
		err = r.sdcClient.RegisterIntegrationOnApplicationAnswer(baepath, *document)
	case "application-withdraw":
		err = r.sdcClient.RegisterApplicationWithdraw(baepath, *document)
	case "application-revocation":
		err = r.sdcClient.RegisterApplicationRevocation(baepath, *document)
	default:
		err = errors.New("invalid document type")
	}
	return err
}

func (r *service) getBasePath(url string) (string, error) {
	suffix := "/api/"

	// Find the starting index of the suffix
	index := strings.Index(url, suffix)
	if index == -1 {
		return "", fmt.Errorf("failed to retrieve basepath")
	}

	// Adjust the start index to the end of the suffix
	index += len(suffix)

	// Extract the desired part of the URL
	result := url[:index]

	return result, nil
}

func (r *service) AddToRetryMechanism(event []byte) error {

	return r.retryHandlerRepo.AddToRetryMechanism(event, r.document)
}
