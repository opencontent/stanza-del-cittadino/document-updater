package services

import (
	"testing"

	"github.com/go-playground/validator"
	"opencitylabs.it/documentupdater/internal/core/domain"
	"opencitylabs.it/documentupdater/logger"
	"opencitylabs.it/documentupdater/sentryutils"
	"opencitylabs.it/documentupdater/utils"
)

func TestIsDocumentProtocolled(t *testing.T) {

	logger := logger.GetLogger()
	defer logger.Sync()

	service := service{
		log:       logger,
		sentryHub: sentryutils.InitLocalSentryhub(),
		validator: validator.New(),
	}

	document, _ := utils.GetDocumentStruct([]byte(DOCUMENT_FULL))

	result := service.isDocumentProtocolled(&document)

	if result != true {
		t.Fatal("Expected true got false")
	}

	documentEmpty := domain.Document{}
	result = service.isDocumentProtocolled(&documentEmpty)

	if result != false {
		t.Fatal("Expected false got true")
	}

}

/* func TestDocumentEncoder(t *testing.T) {

	logger := logger.GetLogger()
	defer logger.Sync()

	service := service{
		log:       logger,
		sentryHub: sentryutils.InitLocalSentryhub(),
		validator: validator.New(),
	}

	document, _ := utils.GetDocumentStruct([]byte(DOCUMENT_FULL))

	err := service.updateApplication(&document)

	if err != nil {
		t.Fatal("Expected no error")
	}
} */

func TestDocumentValidator(t *testing.T) {
	logger := logger.GetLogger()
	defer logger.Sync()

	service := service{
		log:       logger,
		sentryHub: sentryutils.InitLocalSentryhub(),
		validator: validator.New(),
	}
	_, err := service.getBasePath("test")
	if err == nil {
		t.Fatal("Expected  errors: " + err.Error())
	}
	if err.Error() != "failed to retrieve basepath" {
		t.Fatal("Expected  error: failed to retrieve basepath, but got: " + err.Error())
	}
	url := "https://servizi.comune-qa.bugliano.pi.it/lang/api/applications/3e596cd7-2222/attachments/86198e71-2222-4d24-8027-222222?version=1"
	expectedResult := "https://servizi.comune-qa.bugliano.pi.it/lang/api/"

	result, _ := service.getBasePath(url)
	if result != expectedResult {
		t.Fatal("Expected: " + expectedResult + " but got: " + result)
	}

	url2 := "https://test/lang/api/applications/3e596cd7-2222/attachments/86198e71-2222-4d24-8027-222222?version=1"
	expectedResult2 := "https://test/lang/api/"

	result2, _ := service.getBasePath(url2)
	if result2 != expectedResult2 {
		t.Fatal("Expected: " + expectedResult2 + " but got: " + result2)
	}
}
