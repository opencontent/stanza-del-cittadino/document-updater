package domain

type Document struct {
	AppId                 string                 `json:"app_id"`
	EventCreatedAt        string                 `json:"event_created_at"`
	EventId               string                 `json:"event_id"`
	Title                 string                 `json:"title"`
	ID                    string                 `json:"id" validate:"required"`
	Event_Version         int                    `json:"event_version"`
	ExternalID            *string                `json:"external_id,omitempty"`
	RegistrationData      *RegistrationInfo      `json:"registration_data,omitempty"`
	FolderInfo            *FolderInfo            `json:"folder,omitempty"`
	Type                  string                 `json:"type"`
	RemoteID              *string                `json:"remote_id,omitempty"`
	RemoteCollection      *RemoteCollection      `json:"remote_collection,omitempty"`
	Topics                []string               `json:"topics"`
	ShortDescription      string                 `json:"short_description"`
	Description           *string                `json:"description,omitempty"`
	MainDocument          File                   `json:"main_document"`
	ImageGallery          []Image                `json:"image_gallery"`
	HasOrganization       *string                `json:"has_organization,omitempty"`
	Attachments           []File                 `json:"attachments"`
	DistributionLicenseID *string                `json:"distribution_license_id,omitempty"`
	RelatedPublicServices []RelatedPublicService `json:"related_public_services"`
	ValidFrom             *string                `json:"valid_from,omitempty"`
	ValidTo               *string                `json:"valid_to,omitempty"`
	RemovedAt             *string                `json:"removed_at,omitempty"`
	ExpireAt              *string                `json:"expire_at,omitempty"`
	MoreInfo              *string                `json:"more_info,omitempty"`
	NormativeRequirements []string               `json:"normative_requirements"`
	RelatedDocuments      []string               `json:"related_documents"`
	LifeEvents            []string               `json:"life_events"`
	BusinessEvents        []string               `json:"business_events"`
	AllowedReaders        []string               `json:"allowed_readers"`
	TenantID              string                 `json:"tenant_id"`
	OwnerID               string                 `json:"owner_id"`
	DocumentURL           *string                `json:"document_url,omitempty"`
	CreatedAt             string                 `json:"created_at"`
	UpdatedAt             string                 `json:"updated_at"`
	Authors               []Author               `json:"author"`
	SourceType            string                 `json:"source_type"`
	RecipientType         string                 `json:"recipient_type"`
	LastSeen              *string                `json:"last_seen,omitempty"`
	RetryMeta             RetryMeta              `json:"retry_meta"`
}

type RegistrationInfo struct {
	TransmissionType string  `json:"transmission_type"`
	Date             string  `json:"date"`
	DocumentNumber   string  `json:"document_number"`
	RegistryCode     *string `json:"registry_code,omitempty"`
}

type FolderInfo struct {
	Title string `json:"title"`
	ID    string `json:"id"`
}

type File struct {
	Name        string  `json:"name"`
	Description *string `json:"description,omitempty"`
	MimeType    string  `json:"mime_type"`
	URL         string  `json:"url"`
	MD5         string  `json:"md5"`
	Filename    string  `json:"filename"`
	InternalURL *string `json:"internal_url,omitempty"`
}

type Image struct {
	Name        string `json:"name"`
	Filename    string `json:"filename"`
	Description string `json:"description"`
	License     string `json:"license"`
	Type        string `json:"type"`
	URL         string `json:"url"`
}

type RemoteCollection struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

type RelatedPublicService struct {
	ID   string `json:"id"`
	Type string `json:"type"`
	URL  string `json:"url"`
}

type Author struct {
	Type                    string  `json:"type"`
	TaxIdentificationNumber string  `json:"tax_identification_number"`
	Name                    string  `json:"name"`
	FamilyName              *string `json:"family_name,omitempty"`
	StreetName              *string `json:"street_name,omitempty"`
	BuildingNumber          *string `json:"building_number,omitempty"`
	PostalCode              *string `json:"postal_code,omitempty"`
	TownName                *string `json:"town_name,omitempty"`
	CountrySubdivision      *string `json:"country_subdivision,omitempty"`
	Country                 *string `json:"country,omitempty"`
	Email                   string  `json:"email"`
	Role                    string  `json:"role"`
}

type RetryMeta struct {
	//NextAttemptAt string `json:"next_attempt_at"`
	OriginalTopic string `json:"original_topic"`
	//RetryCounter  int    `json:"retry_counter"`
}
