package domain

type Event struct {
	EventID string `json:"event_id"`
	ID      string `json:"id" validate:"required"`
}
