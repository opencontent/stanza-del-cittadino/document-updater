package ports

import "opencitylabs.it/documentupdater/internal/core/domain"

// driver ports
type DocumentUpdaterServiceService interface {
	ProcessMessage(document *domain.Document) error
	AddToRetryMechanism(event []byte) error
}

// driven ports
type RetryMechanismRepository interface {
	AddToRetryMechanism(m []byte, document *domain.Document) error
}

type SdcClientRepository interface {
	RegisterApplication(basepat string, document domain.Document) error
	RegisterApplicationOutcome(basepat string, document domain.Document) error
	RegisterIntegrationOnApplicationRequest(basepat string, document domain.Document) error
	RegisterIntegrationOnApplicationAnswer(basepat string, document domain.Document) error
	RegisterApplicationWithdraw(basepat string, document domain.Document) error
	RegisterApplicationRevocation(basepat string, document domain.Document) error
}

type ProtocolSystem interface {
	GenerateProtocolDocument(document domain.Document) (string, error)
}
