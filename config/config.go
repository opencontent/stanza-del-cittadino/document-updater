package config

import (
	"os"
	"strconv"
	"strings"
	"sync"

	"opencitylabs.it/documentupdater/logger"
)

type Config struct {
	App struct {
		Version               string
		Environment           string
		SupportedEventVersion int
		AppName               string
	}
	Kafka struct {
		KafkaServer   []string
		ConsumerGroup string
		ConsumerTopic string
		ProducerTopic string
	}
	Server struct {
		AddressPort string
		Debug       bool
	}
	Client struct {
		SdcAuthTokenUser string
		SdcAuthTokenPsw  string
	}
	Sentry struct {
		Dsn string
	}
}

var lock = &sync.Mutex{}
var appConfig *Config

func GetConfig() *Config {
	if appConfig != nil {
		return appConfig
	}
	lock.Lock()
	defer lock.Unlock()

	//re-check after locking
	if appConfig != nil {
		return appConfig
	}
	appConfig = initConfig()
	return appConfig
}

func initConfig() *Config {
	logger := logger.GetLogger()
	logger.Debug("loading env vars....")
	var c Config
	c.App.Version = VERSION
	c.App.Environment = getFromEnv("ENVIRONMENT", "test")
	c.App.SupportedEventVersion = 1
	c.App.AppName = "Document-Updater"

	kafkaServer := getFromEnv("KAFKA_SERVER", "kafka:9092")
	c.Kafka.KafkaServer = strings.Split(kafkaServer, ",")
	c.Kafka.ConsumerGroup = getFromEnv("KAFKA_CONSUMER_GROUP", "test")
	c.Kafka.ConsumerTopic = getFromEnv("KAFKA_CONSUMER_TOPIC", "test")
	c.Kafka.ProducerTopic = getFromEnv("KAFKA_PRODUCER_TOPIC", "test")

	c.Server.AddressPort = getFromEnv("SERVER_ADDRESS_PORT", "test")
	c.Server.Debug = getBoolFromEnv("SERVER_DEBUG", false)

	c.Client.SdcAuthTokenUser = getFromEnv("SDC_AUTH_TOKEN_USER", "test")
	c.Client.SdcAuthTokenPsw = getFromEnv("SDC_AUTH_TOKEN_PASSWORD", "test")

	c.Sentry.Dsn = getFromEnv("SENTRY_DSN", "test")
	return &c
}

func getFromEnv(name string, defaultValue string) string {
	logger := logger.GetLogger()
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	logger.Sugar().Debug("using default value for environment var: ", name)
	return defaultValue
}

func getBoolFromEnv(name string, defaultValue bool) bool {
	logger := logger.GetLogger()

	if val, ok := os.LookupEnv(name); ok {
		// Attempt to parse the environment variable as a boolean
		boolValue, err := strconv.ParseBool(val)
		if err != nil {
			// Log an error if parsing fails and return the default value
			logger.Sugar().Errorf("Error parsing boolean value for environment var %s: %v", name, err)
			return defaultValue
		}
		return boolValue
	}

	logger.Sugar().Debug("Using default value for environment var: ", name)
	return defaultValue
}
