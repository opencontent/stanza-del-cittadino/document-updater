package utils

import (
	"encoding/json"
	"errors"

	"opencitylabs.it/documentupdater/internal/core/domain"
)

func GetDocumentStruct(rawData []byte) (domain.Document, error) {
	var documentMessage domain.Document
	if !json.Valid(rawData) {
		return documentMessage, errors.New("invalid JSON syntax")
	}

	if err := json.Unmarshal(rawData, &documentMessage); err != nil {
		return documentMessage, err
	}
	return documentMessage, nil
}

func GetEventIDs(rawData []byte) (domain.Event, error) {
	var event domain.Event
	if !json.Valid(rawData) {
		return event, errors.New("invalid JSON syntax")
	}

	err := json.Unmarshal(rawData, &event)
	if err != nil {
		return event, err
	}
	return event, nil
}
